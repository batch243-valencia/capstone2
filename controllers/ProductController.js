const {Router}= require("express")
const {ProductModel}=require("../models/Products")
const router= Router()
// const Users = require("../models/Users")\
const mongoose = require("mongoose");
const { decode } = require("../auth");
const { SkuModel } = require("../models/Sku");

const createProduct = async (req, res) => {
	// validate if Admin
  const userData = decode(req.headers.authorization);
  if (!userData.isAdmin) return res.status(401).send("Access Denied!, Admin users only!");
try {
	console.log("try");
	const userInput={
		bucket,
		productName,
		description,
		category,
		type,
		material,
		sku
	} = req.body;
	
	// sku.push({"bucket":bucket})
	console.log(sku)
		const newProduct =await ProductModel.create(userInput)
		const newSku =await SkuModel.create(sku)

			return res.status(200).send(
			`NewProduct:${newProduct}
			Sku:${newSku}`);
	} catch (error) {
		// guard if baseSku/name already Been taken
		if(error.message.indexOf("11000") != -1) res.status(409).send("ERROR DUPLICATE ENTRY");
			// error !indexof
	} 
};
// ShowALLACTIVE PRODUCTS
const showAllActiveProducts = async (req, res) => {
	// const prodId = req.params["prodId"];
	try {
		let products = await SkuModel.find({ "isActive": true });

		// Guard Clause NO PRODUCT FOUND
		if(products.length<=0) res.status(200).send("Sorry No Product Found!");

		// Display All Active product
		return res.status(200).send(products);
	} catch (error) {
		return res.send(error.message);
	}
};
// Show PRODUCTS /ADMIN ONLY
const showAllProducts = async (req, res) => {
	const userData = decode(req.headers.authorization);
	// ***************
	// *Guard Clausexyzs!*
	// ***************
	// Validate if Admin
	if (!userData.isAdmin)
		return res.status(401).send("Access Denied!, Admin users only!");
	try {
		let products = await ProductModel.find({});
		res.status(200).send(products);
	} catch (error) {
		res.send((error.message = "No Product Found"));
	}
};
// Update PRODUCTS
const updateProduct = async (req, res) => {
	console.log("UpdateProduct");
	

	const { skuId } = req.params;
	const userData = decode(req.headers.authorization);
	// console.log(userData);
	// Validate if product ID inputted is same as mongoDB format
	if (!mongoose.Types.ObjectId.isValid(skuId))
		return res.status(404).send("No such product exist!");
	if (!userData) return res.send("Access Denied!");
	console.log("invalid ObjectID")
	try {
		if (!userData.isAdmin)
			return res.status(401).send("Access Denied!, Admin users only!");
		const updatedProduct = await SkuModel.findByIdAndUpdate(
			skuId,
			{ ...req.body },
			{ new: true }
		);
		console.log(updatedProduct);
		if (!updatedProduct){
		
			return res.status(404).send("No such product exist!");
			
		}
		return res.status(200).send(updatedProduct);
	} catch (error) {
		return res.status(400).send(error);
	}
};

const archiveProduct = async (req, res) => {
	console.log("archiveProduct");
	const {skuId} = req.params;
	const userData = decode(req.headers.authorization);
	// console.log(userData);
	// Validate if product ID inputted is same as mongoDB format
	if (!mongoose.Types.ObjectId.isValid(skuId))
		return res.status(404).send("No such product exist!");
	if (!userData) return res.send("Access Denied!");
	try {
		console.log("try")
		if (!userData.isAdmin)
			return res.status(401).send("Access Denied!, Admin users only!");

const query = skuId
const update = {
	isActive:false,
	archivedDate:Date.now()
}
const options = {};
const updatedProduct=await SkuModel.findByIdAndUpdate(skuId, update, options);

console.log({updatedProduct})
return res.status(200).send(updatedProduct);
	} catch (error) {
		// console.log("catch")
		return res.status(400).send(error);
	}
};
const unArchive = async (req, res) => {
	console.log("UpdateProduct");
	const {skuId} = req.params;
	const userData = decode(req.headers.authorization);
	// console.log(userData);
	// Validate if product ID inputted is same as mongoDB format
	if (!mongoose.Types.ObjectId.isValid(skuId))
		return res.status(404).send("No such product exist!");
	if (!userData) return res.send("Access Denied!");
	try {
		if (!userData.isAdmin)
			return res.status(401).send("Access Denied!, Admin users only!");

	const query = skuId
	const update = {
		isActive:true,
		archivedDate:null
	}
	const options = {};
	const updatedProduct=await SkuModel.	findByIdAndUpdate(query, update, options);
		console.log(updatedProduct);
		
		return res.status(200).send(updatedProduct);
	} catch (error) {
		return res.status(400).send(error);
	}
};
const showSpecificProduct = async (req, res) => {
	const {userInput} = req.params;
console.log(userInput)
	try {		
		let SpecificSku = await SkuModel.findById(userInput)
		console.log(SpecificSku)
		res.status(200).send(SpecificSku);
	} catch (error) {
		console.log
		res.status(404).send("Product Not Found");
	}
	
};
const searchProductByCategory = async (req, res) => {
	const searchBy = req.params;
	// const userData = decode(req.headers.authorization);
console.log(searchBy)
	try {
		let products = await ProductModel.findOne({ searchBy });
		res.status(200).send(products);
	} catch (error) {
		res.status(404).send("Product Not Found");
	}
};


const nullToEmpty =(temp) =>{return result = temp  || 'new';}

//Validate Field,.. for Required
const validate = (schema, values) => {
  for(field in schema) {
    if(schema[field].required) {
      if(!values[field]) {
        return false;
      }
    }
  }
  return true;
}


/************************************************************************
	Object.keys() GET "Field" value
*************************************************************************
var myObject = { a: 'c', b: 'a', c: 'b' };
var keyNames = Object.keys(myObject);
console.log(keyNames); // Outputs ["a","b","c"]
**/


/************************************************************************
	Object.entries() GET "Value" of Value
*************************************************************************
const credits = { producer: 'John', director: 'Jane', assistant: 'Peter' };
const arr = Object.values(credits);
console.log(arr);
[ 'John', 'Jane', 'Peter' ]
**/

/************************************************************************
	Object.entries() convert a literal object into a key/value pair array
*************************************************************************
const credits = { producer: 'John', director: 'Jane', assistant: 'Peter' };
const arr = Object.entries(credits);
console.log(arr);
[ [ 'producer', 'John' ],
  [ 'director', 'Jane' ],
  [ 'assistant', 'Peter' ]
]
**/







module.exports = {
	createProduct,
	showAllActiveProducts,
	showAllProducts,
	updateProduct,
	archiveProduct,
	unArchive,
	showSpecificProduct,
	searchProductByCategory,
};
