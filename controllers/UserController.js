const User = require("../models/Users");
// const Product = require('../models/Products');
const bcrypt = require("bcrypt");
const auth = require("../auth");

const mongoose = require("mongoose");

const registerUser = async (req, res) => {
	// Initialize Users information
	try {
	let userInput = ({
		userName,
		password,
		email,
		contactNo,
		isAdmin,
		passwordRetrieval,
		firstName,
		middleName,
		lastName,
		billingAddress,
		shippingAddress,
	} = req.body);
	// Encrypt Password
	userInput.password = bcrypt.hashSync(password, 10);
		const newUser = await User.create(userInput)
		return res.status(200).send(newUser);
	} catch (error) {
			if(error.message.indexOf("11000") != -1) res.status(409).send("Acount is already been registered");
	}
};

// async function main() {
//   try {
//     var quote = await getQuote();
//     console.log(quote);
//   } catch (error) {
//     console.error(error);
//   }
// }
const loginUser = async (req, res, next) => {
	const { userInput, password } = req.body;
	try {
		// User Input can be Username or Email Address
		// Same with facebook,steam etc
		// Check if !(empty/undefinrsed) else return status(400)Bad request
		if (!(userInput ||password))
			return res.status(400).send("All input is required");
			const user =  await findUser_UsingUserInput(userInput)
			const isPasswordCorrect =await bcrypt.compareSync(password, user.password)
		if (!user || !isPasswordCorrect) return res.status(400).send("Credentials missmatch")
		const token = await auth.createToken(user);
		res.status(200).send(token);
		// console.log({ token });
	} catch (err) {
		console.log(err);
		return res
			.status(400)
			.send("These credentials do not match our records");
	}
};

// Update User Role/isAdmin
const updateRole = async (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	// console.log(userData);
	const userId = req.params["userId"];
	const changeStatusTo = req.body.changeStatusTo;

	// ***************
	// *Guard Clausexyzs!*
	// ***************
	// Validated User ID
	if (!mongoose.Types.ObjectId.isValid(userId))
		return res.status(400).send("No such user exist!");
	// Validate if Admin
	if (!userData.isAdmin)
		return res.status(401).send("Access Denied!, Admin users only!");

	// TRY to Find USER where "ID" = "UserId"
	// then change "IsAdmin" to "changeStatusTo"
	try {
		
		const userUpdated = await User.findByIdAndUpdate(
			userId,
			{ isAdmin: changeStatusTo },
			{ new: true }
		);
		return res.status(200).send(userUpdated);
	} catch (error) {
		if (err) return res.status(400).send(err);
	}
};

const validateEmail = (email) => {
	return email.match(
		/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
	);
};
// FIND "User" using userInput
// Look if Email Match
// Look if Username Match
const findUser_UsingUserInput = async (userInput) => {
	return await User.findOne({
		$or: [
			{
				email: userInput,
			},
			{
				userName: userInput,
			},
			{
				contactNo: userInput,
			},
		],
	});
};

module.exports = { registerUser, loginUser, updateRole };
// Di ko pla need to hahaha
