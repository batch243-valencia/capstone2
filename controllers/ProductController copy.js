const Products = require("../models/Products");
const Users = require("../models/Users");
const mongoose = require("mongoose");
const { decode } = require("../auth");

const createProduct = async (req, res) => {
	let userInput = ({
		baseSku,
		ProductName,
		description,
		category,
		type,
		material,
	} = req.body);
	console.log(userInput);
	try {
		const newProduct = await Products.push(userInput);
		newProduct.save((err,data)={
			
		})
		res.status(200).json(newProduct);
		console.log("newProduct Success");
	} catch (error) {
		// guard if baseSku/name already Been taken
		if (error.message.indexOf("11000") != -1) {
			// const test = ({ value, field } = JSON.parse(
			// 	error.message.replace(
			// 		"E11000 duplicate key error collection: CapstoneEcommerce.products index: baseSku_1 dup key: ",
			// 		""
			// 	)
			// ));
			// console.log(typeof test);
			// error.message = `Error cannot Register ${value}, Duplicate ${field}`;
			// console.log("Duplicate");
			// return res.send("status Duplicate entry");
			// return res.status(400).send(error.message);
		}
		// error !indexof
		res.status(400).send(error.message);
	}
};
// ShowALLACTIVE PRODUCTS
const showAllActiveProducts = async (req, res) => {
	const prodId = req.params["prodId"];
	try {
		let products = await Products.find({ isActive: true });
		res.status(200).send(products);
	} catch (error) {
		res.send(error.message);
	}
};
// Show ACTIVE PRODUCTS
const showAllProducts = async (req, res) => {
	const userData = decode(req.headers.authorization);
	// ***************
	// *Guard Clausexyzs!*
	// ***************
	// Validate if Admin
	if (!userData.isAdmin)
		return res.status(401).send("Access Denied!, Admin users only!");
	try {
		let products = await Products.find({});
		res.status(200).send(products);
	} catch (error) {
		res.send((error.message = "No Product Found"));
	}
};
// Update PRODUCTS
const updateProduct = async (req, res) => {
	console.log("UpdateProduct");
	const { prodId } = req.params;
	const userData = decode(req.headers.authorization);
	// console.log(userData);
	// Validate if product ID inputted is same as mongoDB format
	if (!mongoose.Types.ObjectId.isValid(prodId))
		return res.status(404).send("No such product exist!");
	if (!userData) return res.send("Access Denied!");
	try {
		if (!userData.isAdmin)
			return res.status(401).send("Access Denied!, Admin users only!");

		const updatedProduct = await Products.findByIdAndUpdate(
			prodId,
			{ ...req.body },
			{ new: true }
		);

		console.log(updatedProduct);
		if (updatedProduct.length < 1)
			return res.status(404).send("No such product exist!");

		return res.status(200).send(updatedProduct);
	} catch (error) {
		return res.status(400).send(error);
	}
};

const archiveProduct = async (req, res) => {
	console.log("UpdateProduct");
	const { prodId } = req.params;
	const userData = decode(req.headers.authorization);
	// console.log(userData);
	// Validate if product ID inputted is same as mongoDB format
	if (!mongoose.Types.ObjectId.isValid(prodId))
		return res.status(404).send("No such product exist!");
	if (!userData) return res.send("Access Denied!");
	try {
		if (!userData.isAdmin)
			return res.status(401).send("Access Denied!, Admin users only!");
		// Archive Data  SET Product to be Deleted
		// after 5 Years!
		const updatedProduct = await Products.findByIdAndUpdate(
			prodId,
			{
				isActive: false,
				archiveDate: Date.now,
			},
			{ new: true }
		);

		console.log(updatedProduct);
		if (updatedProduct.length < 1)
			return res.status(404).send("No such product exist!");

		return res.status(200).send(updatedProduct);
	} catch (error) {
		return res.status(400).send(error);
	}
};
const unArchive = async (req, res) => {
	console.log("UpdateProduct");
	const { prodId } = req.params;
	const userData = decode(req.headers.authorization);
	// console.log(userData);
	// Validate if product ID inputted is same as mongoDB format
	if (!mongoose.Types.ObjectId.isValid(prodId))
		return res.status(404).send("No such product exist!");
	if (!userData) return res.send("Access Denied!");
	try {
		if (!userData.isAdmin)
			return res.status(401).send("Access Denied!, Admin users only!");

		const updatedProduct = await Products.findByIdAndUpdate(
			prodId,
			{
				isActive: true,
				archiveDate: "",
			},
			{ new: true }
		);

		console.log(updatedProduct);
		if (updatedProduct.length < 1)
			return res.status(404).send("No such product exist!");

		return res.status(200).send(updatedProduct);
	} catch (error) {
		return res.status(400).send(error);
	}
};
const showSpecificProduct = async (req, res) => {
	const prodId = req.params["prodId"];
	// const userData = decode(req.headers.authorization);

	try {
		let products = await Products.findOne({ _id: prodId, isActive: true });
		res.status(200).send(products);
	} catch (error) {
		res.status(404).send("Product Not Found");
	}
};

const searchProductByCategory = async (req, res) => {
	const searchBy = req.params;
	// const userData = decode(req.headers.authorization);

	try {
		let products = await Products.findOne({ searchBy });
		res.status(200).send(products);
	} catch (error) {
		res.status(404).send("Product Not Found");
	}
};

module.exports = {
	createProduct,
	showAllActiveProducts,
	showAllProducts,
	updateProduct,
	archiveProduct,
	unArchive,
	showSpecificProduct,
	searchProductByCategory,
};
