const UserModel = require("../models/Users");
const {OrderModel} = require("../models/Orders");
const {ProductModel}=require("../models/Products");
const {CartModel}=require("../models/Cart");
const { default: mongoose } = require("mongoose");
const { decode } = require("../auth");
const { SkuModel } = require("../models/Sku");



// const { update } = require("../models/Users");


const showCart = async (req, res) => {

  try {
    const {id,isAdmin} = decode(req.headers.authorization);
       if (isAdmin) return res.status(401).send("Access Denied!, You are logged in as Admin!");
      const cart= await CartModel.find({user_id:id})
      if (cart.length<=0){res.send("Cart is Empty")}
      return res.status(200).send(cart);
  } catch (error) {
    console.log(error.message)
  }
      
      
}
//******************************************** */
const addToCart = async (req, res) => {
      const {id:userId,isAdmin} = decode(req.headers.authorization);
      // console.log(id)
      const {prodId,sku:sku_id,quantity} = req.body
      // console.log(`prodId:${prodId}!`)
      //validate ProdID
      if (!mongoose.Types.ObjectId.isValid(sku_id))
                  return res.status(404).send("No such product exist!");

      if (isAdmin) return res.status(401).send("Access Denied!, You are logged in as Admin!");
      const skuItem = await SkuModel.findById(sku_id)


      if (!skuItem) return res.status(404).send("No such product exist!");

      if (skuItem.stocks <= 0) {
      return res.status(200).send(`${skuItem.bucket} is out of stocks!`);
      }

       if (quantity > skuItem.stocks)
      return res.status(200).send("stocks is insufficient");

      if (!skuItem.isActive)
      return res
        .status(200)
        .send("Product is currently Unavailable");
        
      const {price} = skuItem
      const totalAmount = quantity * price;
   
    
// ******************** if cart already exist
    try {
      const ifAlreadyExist = await CartModel.find({"$and": [{"user_id":userId},{"sku_id":sku_id}]});
    
      
        ifAlreadyExist.quantity = quantity;
        if (ifAlreadyExist.length >0){
        ifAlreadyExist.save
      return res.status(200).send("Quantity Updated"+ifAlreadyExist);}
    } catch (error) {
      console.log(error.message)
      // console.log("ifalreadyexist = false");
    }
      
// ******************** create new cart
      const cart = await CartModel.create({
      user_id: userId,
      product_id:prodId,
      sku_id:sku_id,
      quantity:quantity,
      price:price
    });
    return res.status(200).send(cart);

   

      // console.log(existingCart)
      // const cartItem = {
      //       "user_id":userId,
		
      //       "product_id":product._id.toString(),
	// 	"sku_id":sku_id,
	// 	"quantity":quantity,
	// 	"price":product.sku[0].price
      // }
      //      console.log(cartItem)
      // const cart = await CartModel.updateOne
      // cart.user_
      // if (user.cart.includes(sku)) return res.send("Duplicate Item");
      // res.send(existingCart)
      
      // const user = await UserModel.sku.findById(id);
      // console.log(product);


      // const sku = product.includes(skuId)
      // console.log(sku)
      // const cartItem ={
      //          "price": products.price,
      //           "stocks": 10,
      //           "colors": "Canvas",
      //           "sizes": "100x100m",
      //           "weight": "100grams",
      //           "isActive": true,
      //           "archivedDate": null,
      //             }
      // user.cart.push()
      // const bucketSKU = await products.includes(sku)
      // return res.status(200).send(UserData.cart)
            // const sku = await product.sku.findById()
		// const updatedCart = await UserModel.findById(id)
            

            // if (updatedCart.cart[0].includes({"prodId":prodId})){
            //       console.log("dup")
            //       res.send("Duplicate Product")}      
            // updatedCart.cart.push(cart)
            // updatedCart.save()
		// return res.status(200).send(updatedCart.cart);
      // } catch (error) {
      //       console.log("try")
      //       console.log(error.message)
      //       // return res.status(404).send("No such product exist!");
      // }     
}
//******************************************** */
const removeFromCart = async (req, res) => {

      return res.status(200).send("ShowCart");
}
//******************************************** */
const clearCart = async (req, res) => {

      const {id} = decode(req.headers.authorization);
      return res.status(200).send("cart emptied")
}

//******************************************** */
const checkout =  (req, res) => {

 
  const {id:UserId, isAdmin} = decode(req.headers.authorization);

  // // Validate if user ID inputted is same as mongoDB format
  // if (!mongoose.Types.ObjectId.isValid(userId))
  //   return res.status(404).send("No such user exist!");

  try {
    if (isAdmin)
    return res.status(401).send("Access Denied!, Regular users only!");

    
    const carts = CartModel.find({ user_id: UserId });
    // console.log(carts)
    if (carts.length <= 0)
      return res.status(404).send("No such cart exist on this user!");

    const products = [];
    let totalAmount = 0;
    
    const productIds = [];
// 
    for (let i = 0; i < carts.length; i++) {
      totalAmount += carts[i].totalAmount;
      products.push({
        user_id: carts[i].user_id,
        product_id: carts[i].product_id,
        sku_id: carts[i].sku_id,
        quantity: carts[i].quantity,
        price: carts[i].price,
      });
      productIds.push(carts[i].product_id);
    }
  

    const OrderItems = { user_id: UserId,
      items:products,
      totalAmount,}
    const order =  OrderModel.create(OrderItems);

      console.log(`${OrderItems} ${totalAmount}`)
// console.log(UserId)
    CartModel.deleteMany({user_id: UserId});
    console.log("order")
    return res.status(200).send(order);
  } catch (error) {
    return res.status(400).send(error.message);
  }
}

// if (carts.length < 1) return res.status(404).send("No such cart exist!");

module.exports = 
	{showCart,
	addToCart,
	removeFromCart,
	clearCart,
	checkout
}