const  mongoose = require('mongoose');
const schema = mongoose.Schema;

const cartSchema = new schema(
	{
        user_id:"String",
		product_id:"String",
		sku_id:"String",
		quantity:"Number",
		price:"Number"
	}
)

const CartModel =mongoose.model("Cart",cartSchema)
module.exports = {CartModel}