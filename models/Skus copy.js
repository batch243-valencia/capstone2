const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const skuSchema = new Schema({
	// Stock keeping unit

	sku_Id: [
		{
			price: {
				type: "NumberDecimal",
			},
		}, //2decimalpoints
		{
			stocks: {
				type: "Number",
			},
		},
		{
			colors: {
				type: "String",
			},
		},
		{
			weight: {
				type: "String",
			},
		},
		{
			sizes: {
				type: "string",
			},
		},
	],

	createdOn: { type: Date, default: Date.now },
});

module.exports = mongoose.model("Sku", skuSchema);
