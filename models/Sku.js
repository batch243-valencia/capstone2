const  mongoose = require('mongoose');
const schema = mongoose.Schema;
const skuSchema = new schema({		
		bucket: {
		type: "String",
		},
        price: {
		type: "Number",
		},
		stocks: {
			type: "Number",
			default: 0,
		},
		colors: {
			type: "String"
		},
		sizes: {
			type: "string"
		},
		weight: {
			type: "String"
		},
		isActive: {
			type: Boolean,
			default: true
		},
		createdOn: {
			type: Date,
			default: Date.now
		},
		archivedDate: {
			type: Date,
			default: ""
		}
	}
)
const SkuModel =mongoose.model("Sku",skuSchema)
module.exports = {SkuModel}