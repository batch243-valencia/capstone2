
const  mongoose = require('mongoose');
const schema = mongoose.Schema;


const OrderSchema = schema({
    user_id: "String",
    items:[{
        product_id: "String",
        sku_id: "String",
        quantity: "Number",
        price: "Decimal",
    }],
    totalAmount:"Decimal"
}
);

const OrderModel =mongoose.model("Order",OrderSchema)
module.exports = {OrderModel}