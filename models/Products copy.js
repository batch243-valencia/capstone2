const  mongoose = require('mongoose');
const schema = mongoose.Schema;


const skuSchema = new schema({
  // Stock keeping unit
  		_id: mongoose.Types.ObjectId,
		price: {
		type: "Number",
		required: true
		},
  		stocks: {
			type: "Number",
			default: 0,
			required: true
  		},
      	colors: {
        	type: "String"
      	},
      	sizes: {
        	type: "string"
      	},
		weight: {
        	type: "String"
      	},
		isActive: {
			type: Boolean,
			default: true
  		},
		createdOn: {
			type: Date,
			default: Date.now
  		},
	// 	Products:
	// 	{
	// 	type:Schema.Types.ObjectId,
	// 	ref: 'Products'
	// 	}
    }
)
const productsSchema = new schema({
	bucket: {
		type: "String",
		required: [true, "Please add baseSku"],
		unique: [true, "Duplicate SKU, think of another one"],
	},
	productName: {
		type: "String",
		required: [true, "{Please Add a Product Name"],
		unique: [true, "Duplicate Product Name, Think Different"],
	},
	description: [{
		type: "String",
	}],
	category: {
		type: "String",
		lowercase: true,
	},
	type: { type: "String" },
	materials: [{
		type: "String"
	}],	
	sku:[{
		type:schema.Types.ObjectId,
		ref: 'Sku'
		}]

	}

);

const ProductModel =mongoose.model("Product",productsSchema)
const SkuModel = mongoose.model("Sku", skuSchema)
module.exports = {ProductModel,SkuModel}

field = {fieldname:value}//param value
const newProduct =  ProductModel({newProduct	})
sku = {...sku} //param value
newProduct.sku.push({sku})
newProduct.save((err,data)=>{});
// const ImageSchema = mongoose.Schema({
//     type: String,
//     data: Buffer
// });

// module.exports = mongoose.model(
//     "ImageSchema",schema
// );
