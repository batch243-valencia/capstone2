const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const commentSchema = new mongoose.Schema({
    text:String,
    username:String,
});

const postSchema = new Schema({
    text:String,
    username: String,
    comments: [{type:Schema.Types.ObjectId,ref:'comment'}]
});
const PostModel = mongoose.model('post', postSchema);
const CommentModel = mongoose.model('comment', commentSchema);

const aPost = new PostModel({text: 'Hi', username: 'Aaron'});
aPost.comments.push({username:"Bob", text:"Great Post!"})

aPost.save((err,data)=>{

})
