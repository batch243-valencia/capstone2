const mongoose = require("mongoose");

const usersSchema = new mongoose.Schema({
	userName: {
		type: "String",
		unique: true,
		required: true,
	},
	password: {
		type: "String",
		required: true,
	},
	email: {
		type: "String",
		unique: true,
		required: true,
	},
	contactNo: {
		type: "String",
		required: true,
		unique: true,
	},
	isAdmin: {
		type: Boolean,
		required: true,
		default: false,
	},
	passwordRetrieval: [
		{ question1: "String", answer1: "String" },
		{ question2: "String", answer2: "String" },
	],
	firstName: {
		type: "String",
		required: true,
	},
	middleName: String,
	lastName: {
		type: "String",
		required: true,
	},
	billingAddress: {
		street: {
			type: "String",
			// required: [
			// 	true,
			// 	"Please input your Unit Number + House/Building/Street Number",
			// ],
		},
		district: "String",
		barangay: "String",
		cityOrMunicipality: {
			type: "String",
			// required: [true, "Please input your City or Municipality"],
		},
		province: "String",
		zip: "String",
		country: {
			type: "String",
		// 	required: true,
		},
	},
	shippingAddress: {
		street: {
			type: "String",
			// required: [
			// 	true,
			// 	"Please input your Unit Number + House/Building/Street Number",
			// ],
		},
		district: "String",
		barangay: "String",
		cityOrMunicipality: {
		// 	type: "String",
		// 	required: [true, "Please input your City or Municipality"],
		},
		province: "String",
		zip: "String",
		country: {
		// 	type: "String",
		// 	required: true,
		},
	}
}	
);
module.exports = mongoose.model("Users", usersSchema);
