// Im plannning to remove all Vowel
// const skuGenerator = (str) => {
//     var vowelIndex = str.indexOf(str.match(/[aeiou]/)); //returns index of first vowel in str
//         return "r" + str.slice(vowelIndex);
// }
// console.log(skuGenerator('xylographer'));

const  mongoose = require('mongoose');
const schema = mongoose.Schema;

const productsSchema = schema({
	bucket: {
		type: "String",
 		unique: true,
		 min: [3,"minimum or 3 Characters"],
 		required: [true, "Please add baseSku"],
	},
	productName: {
		type: "String",
		unique: [true, "Duplicate Product Name, Think Different"],
		min: [3,"minimum or 3 Characters"],
		required: [true, "{Please Add a Product Name"],		
	},
	description: [{
		min: [3,"minimum or 3 Characters"],
		type: "String",
	}],
	category: {
		type: "String",
		lowercase: true,
		required:true,
	},
	type: {
		type: "String",
		lowercase: true,
		required:true,
	},
	materials: [{
		type: "String"
	}]
})

const ProductModel =mongoose.model("Product",productsSchema)
module.exports = {ProductModel}

// const ImageSchema = mongoose.Schema({
//     type: String,
//     data: Buffer
// });

// module.exports = mongoose.model(
//     "ImageSchema",schema
// );
