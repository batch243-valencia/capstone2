// import { sign, verify, decode, JsonWebTokenError } from "jsonwebtoken";
require("dotenv").config();
const jwt = require("jsonwebtoken");

const verifyIfAdmin = () => {
	//Verify If Admin
	const userData = auth.decode(req.headers.authorization);
	!userData.isAdmin ? false : true;
};

const createToken = (user) => {
	// Payload of JWT
	const data = {
		id: user._id,
		userName: user.userName,
		email: user.email,
		isAdmin: user.isAdmin,
	};
	// Generate a JSON web token using the jwt's sign method
	return jwt.sign(data, process.env.TOKEN_KEY, {});
};

const verify = (req, res, next) => {
	let token = req.headers.authorization;

	try {
		// Validate the "token" using verify method, to decrypt the token using the secret code.

		if (!token) {
			return res.send("Authentication failed! No token provided");
		}

		token = token.replace("Bearer ", "");
		return jwt.verify(token, process.env.TOKEN_KEY, (error) => {
			if (error) {
				return res.send("Invalid Token");
			} else {
				next();
			}
		});
	} catch (error) {
		console.log(`Im in verify \n${error.message}`);
		res.send(error.message);
	}
};

const decode = (token) => {
	if (!token) {
		return null;
	} else {
		token = token.replace("Bearer ", "");

		return jwt.verify(token, process.env.TOKEN_KEY, (error, data) => {
			if (error) {
				return null;
			} else {
				return jwt.decode(token, { complete: true }).payload;
			}
		});
	}
};

module.exports = { createToken, verify, decode, verifyIfAdmin };
