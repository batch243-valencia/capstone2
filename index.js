require("dotenv").config();
console.log(process.env);
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// Allows all resources to access our backend application
const app = express();
// middleware

app.use(cors());
// app.use(cors());
app.use(express());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use((req, res, next) => {
	console.log(req.path, req.method);
	next();
});

// **********************
// ********ROUTES********
// **********************
const userRoutes = require("./routes/userRoutes");
app.use("/user", userRoutes);
const productRoutes = require("./routes/productRoutes");
app.use("/product", productRoutes);

const cartRoutes = require("./routes/cartRoutes");
app.use("/cart", cartRoutes);

//check/Initialize Connection
mongoose.connection.on(
	"error",
	console.error.bind(console, "connection error")
);
mongoose.connection.once("open", () =>
	console.log("Now Connected to MongoDb Atlas")
);

mongoose.set({ strictQuery: false });
mongoose
	.connect(process.env.DATABASE_URL, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
	})
	.then(() => {
		console.log("connected to database");
		// listen to port
		const port = process.env.PORT;
		app.listen(process.env.PORT, () => {
			console.log(`listening for requests on port localhost:${port}`);
		});
	})
	.catch((err) => {
		console.log(err);
	});
