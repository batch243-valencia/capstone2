
require("dotenv").config();
const express = require('express')
const router = express.Router();
const auth = require("../auth");

//require cart Controller
const {
	showCart,
	addToCart,
	removeFromCart,
	clearCart,
	checkout
} = require("../controllers/CartController");
// /search SKU bucket/color/size/
router.get("/",auth.verify, showCart);
router.patch("/AddToCart/", auth.verify, addToCart);
router.patch("/RemoveFromCart/:id", auth.verify, removeFromCart);
// router.patch("/RemoveFromCart/:prodId/:sku", auth.verify, removeFromCart);
router.post("/clear", auth.verify, clearCart);
router.post("/checkout/", auth.verify, checkout);
module.exports = router;

