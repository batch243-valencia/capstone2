
require("dotenv").config();
const express = require('express')
const router = express.Router();
const auth = require("../auth");


const {
	createProduct,
	showAllActiveProducts,
	showAllProducts,
	updateProduct,
	archiveProduct,
	unArchive,
	showSpecificProduct,
	searchProductByCategory,
} = require("../controllers/ProductController");

// const auth = "../auth";

router.post("/newProduct", auth.verify, createProduct);
router.get("/", showAllActiveProducts);
router.get("/showAllProducts", auth.verify, showAllProducts);

router.patch("/UpdateProduct/:skuId", auth.verify, updateProduct);
router.patch("/ArchiveProduct/:skuId", auth.verify, archiveProduct);
router.patch("/UnArchive/:skuId", auth.verify, unArchive);
// router.get("/search", showSpecificProduct);
router.get("/:userInput", auth.verify,showSpecificProduct);
// /search SKU bucket/color/size/
router.get("/search/:bucket/:color/:size", showSpecificProduct);

module.exports = router;
