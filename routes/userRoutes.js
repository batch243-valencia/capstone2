const express = require("express");
require("dotenv").config();
const router = express.Router();
const auth = require("../auth");

const {
	updateRole,
	loginUser,
	registerUser,
} = require("../controllers/UserController");
const { createToken, verify, decode } = "../auth";

router.post("/login", loginUser);
router.post("/register", registerUser);
router.patch("/updateRole/:userId", auth.verify, updateRole);
// GET all Users
// router.get('/', getUsers)

// // GET a single Users
// router.get('/:id', getUsers)

// // POST a new Users
// router.post('/', createUsers)

// // DELETE a Users
// router.delete('/:id', deleteUsers)

// // UPDATE a Users
// router.patch('/:id', updateUsers)

module.exports = router;
